<div class="modal-header">
	<h4 class="modal-title">Add</h4>
</div>
<form name="Form" id="form1" novalidate ng-submit="addData(Form, myModel)" >
	<div class="modal-body">

		<div class="row">
			<div class="col-md-12">
				<div class="form-group" ng-class="{'has-error':Form.designation.$dirty && Form.designation.$invalid, 'has-success':Form.designation.$valid}">
					<label class="control-label">
						DESIGNATION <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter  DESIGNATION" class="form-control" name="designation" ng-model="myModel.designation" required />
					<span class="error text-small block" ng-if="Form.designation.$dirty && Form.designation.$invalid">DESIGNATION is required</span>
					<span class="success text-small" ng-if="Form.designation.$valid">Thank You!</span>
				</div>

				<div class="form-group" ng-class="{'has-error':Form.inclusiveDate.$dirty && Form.inclusiveDate.$invalid, 'has-success':Form.inclusiveDate.$valid}" >
					<label class="control-label">
						INCLUSIVE DATE <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter  INCLUSIVE DATE" class="form-control" name="inclusiveDate" ng-model="myModel.inclusiveDate" required />
					<span class="error text-small block" ng-if="Form.inclusiveDate.$dirty && Form.inclusiveDate.$invalid">INCLUSIVE DATE is required</span>
				</div>

				<div class="form-group" ng-class="{'has-error':Form.unit.$dirty && Form.unit.$invalid, 'has-success':Form.unit.$valid}">
					<label class="control-label">
						UNIT <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter UNIT" class="form-control" name="unit" ng-model="myModel.unit" required />
					<span class="error text-small block" ng-if="Form.unit.$dirty && Form.unit.$invalid">UNIT is required</span>
					<span class="success text-small" ng-if="Form.unit.$valid">Wonderful!</span>
				</div>
			</div>
		</div>



	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary" >OK</button>
		<button class="btn btn-primary btn-o" ng-click="cancel()">Cancel</button>
	</div>
</form>
