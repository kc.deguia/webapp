<div class="thumbnail" style="border:none !important">
    <img src="{[{myModel.familyPic ? myModel.familyPic : 'be/images/default.jpg' }]}" alt="">
    <div class="caption">
        <p>
            <a class="btn btn-wide btn-primary" ng-click="form.upload('lg', 'upload.html', 'familyPic', myModel); myModel.temp"><i class="glyphicon glyphicon-share"></i></a>
        </p>
    </div>
</div>
