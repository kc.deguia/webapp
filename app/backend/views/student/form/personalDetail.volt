<div class="row">
    <div class="col-md-4">
        <fieldset>
            <legend>
                Upload Photo
            </legend>
            <div class="row">

                <input hidden="true" ng-model="myModel.temp" ng-init="myModel.temp = 'default'" />
                <div class="col-xs-6 col-xs-offset-3">
                    <img src="{[{myModel.profile ? myModel.profile : 'be/images/default.jpg' }]}" class="img-rounded img-responsive margin-bottom-15" alt="A generic image with rounded corners">
                        <a class="btn btn-wide btn-primary" ng-click="form.upload('lg', 'upload.html', 'profile', myModel); myModel.temp">
                            <i class="glyphicon glyphicon-share"></i>
                        </a>
                </div>


            </div>
        </fieldset>
    </div>
    <div class="col-md-4">
        <div class="form-group" ng-class="{'has-error':Form.fname.$dirty && Form.fname.$invalid, 'has-success':Form.fname.$valid}">
            <label class="control-label">
                First Name <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter  First Name" class="form-control" name="fname" ng-model="myModel.fname" required />
            <span class="error text-small block" ng-if="Form.fname.$dirty && Form.fname.$invalid">First Name is required</span>
            <span class="success text-small" ng-if="Form.fname.$valid">Thank You!</span>
        </div>

        <div class="form-group" >
            <label class="control-label">
                Middle Name
            </label>
            <input type="text" placeholder="Enter  Middle Name" class="form-control" name="mname" ng-model="myModel.mname"  />
            <span class="error text-small block" ng-if="Form.mname.$dirty && Form.mname.$invalid">Middle Name is required</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.lname.$dirty && Form.lname.$invalid, 'has-success':Form.lname.$valid}">
            <label class="control-label">
                Last Name <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter  Last Name" class="form-control" name="lname" ng-model="myModel.lname" required />
            <span class="error text-small block" ng-if="Form.lname.$dirty && Form.lname.$invalid">Last Name is required</span>
            <span class="success text-small" ng-if="Form.lname.$valid">Wonderful!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.rank.$dirty && Form.rank.$invalid, 'has-success':Form.rank.$valid}">
            <label class="control-label">
                Rank <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Rank" class="form-control" name="rank" ng-model="myModel.rank" required />
            <span class="error text-small block" ng-if="Form.rank.$dirty && Form.rank.$invalid">Last Name is required</span>
            <span class="success text-small" ng-if="Form.rank.$valid">Wonderful!</span>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group" ng-class="{'has-error':Form.afpsn.$dirty && Form.afpsn.$invalid, 'has-success':Form.afpsn.$valid}">
            <label class="control-label">
                AFPSN <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter AFPSN" class="form-control" name="afpsn" ng-model="myModel.afpsn" required />
            <span class="error text-small block" ng-if="Form.afpsn.$dirty && Form.afpsn.$invalid">Last Name is required</span>
            <span class="success text-small" ng-if="Form.aafpsn.$valid">Wonderful!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.afpIdNo.$dirty && Form.afpIdNo.$invalid, 'has-success':Form.afpIdNo.$valid}">
            <label class="control-label">
                AFP ID No. <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter AFP ID No." class="form-control" name="afpIdNo" ng-model="myModel.afpIdNo" required />
            <span class="error text-small block" ng-if="Form.afpIdNo.$dirty && Form.afpIdNo.$invalid">Last Name is required</span>
            <span class="success text-small" ng-if="Form.aafpsn.$valid">Wonderful!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.brSvcAfpos.$dirty && Form.brSvcAfpos.$invalid, 'has-success':Form.brSvcAfpos.$valid}">
            <label class="control-label">
                Br of Svc/AFPOS <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Br of Svc/AFPO" class="form-control" name="brSvcAfpos" ng-model="myModel.brSvcAfpos" required />
            <span class="error text-small block" ng-if="Form.brSvcAfpos.$dirty && Form.brSvcAfpos.$invalid">Last Name is required</span>
            <span class="success text-small" ng-if="Form.aafpsn.$valid">Wonderful!</span>
        </div>
    </div>


</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group" ng-class="{'has-error':Form.homeAddress.$dirty && Form.homeAddress.$invalid, 'has-success':Form.homeAddress.$valid}">
            <label class="control-label">
                Present Home Address <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Present Home Address" class="form-control" name="homeAddress" ng-model="myModel.homeAddress" required />
            <span class="error text-small block" ng-if="Form.homeAddress.$dirty && Form.homeAddress.$invalid">Present Home Address is required</span>
            <span class="success text-small" ng-if="Form.homeAddress.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.city.$dirty && Form.city.$invalid, 'has-success':Form.city.$valid}">
            <label class="control-label">
                City <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter City" class="form-control" name="city" ng-model="myModel.city" required />
            <span class="error text-small block" ng-if="Form.city.$dirty && Form.city.$invalid">City is required</span>
            <span class="success text-small" ng-if="Form.city.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.province.$dirty && Form.province.$invalid, 'has-success':Form.province.$valid}">
            <label class="control-label">
                Province <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Province" class="form-control" name="province" ng-model="myModel.province" required />
            <span class="error text-small block" ng-if="Form.province.$dirty && Form.province.$invalid">Province is required</span>
            <span class="success text-small" ng-if="Form.province.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.region.$dirty && Form.region.$invalid, 'has-success':Form.region.$valid}">
            <label class="control-label">
                Region <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Regione" class="form-control" name="region" ng-model="myModel.region" required />
            <span class="error text-small block" ng-if="Form.region.$dirty && Form.region.$invalid">Region is required</span>
            <span class="success text-small" ng-if="Form.region.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.birthdate.$dirty && Form.birthdate.$invalid, 'has-success':Form.birthdate.$valid}">
            <label class="control-label">
                Date of Birth <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="yy-mm-dd" class="form-control" name="birthdate" ng-model="myModel.birthdate" required />
            <span class="error text-small block" ng-if="Form.birthdate.$dirty && Form.birthdate.$invalid">Date of Birth is required</span>
            <span class="success text-small" ng-if="Form.birthdate.$valid">Thank You!</span>
        </div>

    </div>

    <div class="col-md-4">

        <div class="form-group" ng-class="{'has-error':Form.placeOfBirth.$dirty && Form.placeOfBirth.$invalid, 'has-success':Form.placeOfBirth.$valid}">
            <label class="control-label">
                Place of Birth <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Place of Birth" class="form-control" name="placeOfBirth" ng-model="myModel.placeOfBirth" required />
            <span class="error text-small block" ng-if="Form.placeOfBirth.$dirty && Form.placeOfBirth.$invalid">Place of Birth is required</span>
            <span class="success text-small" ng-if="Form.placeOfBirth.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.nickName.$dirty && Form.nickName.$invalid, 'has-success':Form.nickName.$valid}">
            <label class="control-label">
                Nickname <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Nickname" class="form-control" name="nickName" ng-model="myModel.nickName" required />
            <span class="error text-small block" ng-if="Form.nickName.$dirty && Form.nickName.$invalid">Nickname is required</span>
            <span class="success text-small" ng-if="Form.nickName.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.religion.$dirty && Form.religion.$invalid, 'has-success':Form.religion.$valid}">
            <label class="control-label">
                Religion <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Religion" class="form-control" name="religion" ng-model="myModel.religion" required />
            <span class="error text-small block" ng-if="Form.religion.$dirty && Form.religion.$invalid">Religion is required</span>
            <span class="success text-small" ng-if="Form.religion.$valid">Thank You!</span>
        </div>


        <div class="form-group" ng-class="{'has-error':Form.gsisNo.$dirty && Form.gsisNo.$invalid, 'has-success':Form.gsisNo.$valid}">
            <label class="control-label">
                GSIS No. <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter GSIS No." class="form-control" name="gsisNo" ng-model="myModel.gsisNo" required />
            <span class="error text-small block" ng-if="Form.gsisNo.$dirty && Form.gsisNo.$invalid">GSIS No. is required</span>
            <span class="success text-small" ng-if="Form.gsisNo.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.sssNo.$dirty && Form.sssNo.$invalid, 'has-success':Form.sssNo.$valid}">
            <label class="control-label">
                SSS No. <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter SSS No. " class="form-control" name="sssNo" ng-model="myModel.sssNo" required />
            <span class="error text-small block" ng-if="Form.sssNo.$dirty && Form.sssNo.$invalid">SSS No. is required</span>
            <span class="success text-small" ng-if="Form.sssNo.$valid">Thank You!</span>
        </div>



    </div>


    <div class="col-md-4">

        <div class="form-group" ng-class="{'has-error':Form.tinNo.$dirty && Form.tinNo.$invalid, 'has-success':Form.tinNo.$valid}">
            <label class="control-label">
                TIN # <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter TIN #" class="form-control" name="tinNo" ng-model="myModel.tinNo" required />
            <span class="error text-small block" ng-if="Form.tinNo.$dirty && Form.tinNo.$invalid">TIN # is required</span>
            <span class="success text-small" ng-if="Form.tinNo.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.bloodType.$dirty && Form.bloodType.$invalid, 'has-success':Form.bloodType.$valid}">
            <label class="control-label">
                Blood Type <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Blood Type" class="form-control" name="bloodType" ng-model="myModel.bloodType" required />
            <span class="error text-small block" ng-if="Form.bloodType.$dirty && Form.bloodType.$invalid">Blood Type is required</span>
            <span class="success text-small" ng-if="Form.bloodType.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.contactNo.$dirty && Form.contactNo.$invalid, 'has-success':Form.contactNo.$valid}">
            <label class="control-label">
                Telephone Number: <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Telephone Number" class="form-control" name="contactNo" ng-model="myModel.contactNo" required />
            <span class="error text-small block" ng-if="Form.contactNo.$dirty && Form.contactNo.$invalid">Telephone Number: is required</span>
            <span class="success text-small" ng-if="Form.contactNo.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.email.$dirty && Form.email.$invalid, 'has-success':Form.email.$valid}">
            <label class="control-label">
                E-mail <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter E-mail" class="form-control" name="email" ng-model="myModel.email" required />
            <span class="error text-small block" ng-if="Form.email.$dirty && Form.email.$invalid">E-mail is required</span>
            <span class="success text-small" ng-if="Form.email.$valid">Thank You!</span>
        </div>
    </div>

</div>
