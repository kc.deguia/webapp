<?php
namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    public function beforeExecuteRoute(Dispatcher $dispatcher){
        // /* 1st We get the Controller name of the Module and the Action name*/
        // $controllerName = $dispatcher->getControllerName();
        // $actionName = $dispatcher->getActionName();
        // /* 2rd then we will check if the module is turned on */
        // $curl = curl_init($this->config->application->ApiURL."/settings/modules/".$controllerName."/".$actionName);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // $curl_response = curl_exec($curl);
        // if ($curl_response === false) {
        //     $info = curl_getinfo($curl);
        //     curl_close($curl);
        //     die('error occured during curl exec. Additioanl info: ' . var_export($info));
        // }
        // $decoded = json_decode($curl_response);
        // curl_close($curl);
        // /* we will now redirect the user to the 404 page if the module status is turned off
        //     $decoded->data[0] : returns 'true' or 'false'
        // */
        // if($decoded!=NULL && $decoded->data[0]=='false'){
        //     $this->response->redirect("admin/admin/page404");
        // } /* ELSE DO NOTHING */
    }

    protected function initialize()
    {
        /* Generate a config.js */
        $this->createJsConfig();
        $this->tag->setTitle("Master Site");
    }

    private function createJsConfig() {
        $script="app.constant('Config', {
            // This is a generated Config
            // Any changes you make in this file will be replaced
            // Place your changes in /dist/config/config.yours.php file \n";
            foreach( $this->config->application as $key=>$val){
                $script .= $key . ' : "' . $val .'",' . "\n";
            }
        $script .= "});";
        $fileName="../public/be/js/scripts/config.js";

        $exist = file_get_contents($fileName);
        if($exist != $script) {

            unlink(__DIR__ . '/../../' . $fileName);

            file_put_contents($fileName, $script);

        }
    }

}
