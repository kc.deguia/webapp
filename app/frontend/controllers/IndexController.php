<?php

namespace Modules\Frontend\Controllers;

use \Phalcon\Mvc\View;

class IndexController extends ControllerBase
{

    public function onConstruct(){

    }

    public function indexAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }


}
