'use strict';
app.directive('fileReader', function() {
	return {
        scope: {
            fileReader: "="
        },
		link: function(scope, elem, attr) {
            elem.bind("change", function (evt) {
                var file = evt.currentTarget.files[0];
                var reader = new FileReader();
                reader.onload = function (evt) {
                    scope.$apply(function () {
                        scope.fileReader =  evt.target.result;
                    });
                };
                reader.readAsDataURL(file);
            });
		}
	};
});

app.directive('imageSrc', function() {
	return {
        scope: {
            imgSrc: "=imageSrc"
        },
		link: function(scope, elem, attr) {

            scope.$watch('imgSrc', function(oldValue, newValue){
                if(newValue){
                    attr.$set('ng-src', newValue)
                }
            }, true)
		}
	};
});
