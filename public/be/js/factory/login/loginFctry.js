app.factory('loginFctry', function($http, $q, Config){
    return {
        memberlogin: function(memberdata, callback){
            $http({
                url: "/members/memberlogin",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: memberdata
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);
            });
        }

    }
})