app.factory('mainFactory', function($http, $q, Config, store){
    return {
        refreshtoken: function(membertoken, callback){
            $http({
                url: Config.ApiURL +"/members/refreshtoken",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(membertoken)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        getloginstatus: function(postdata, callback){
            $http({
                url: Config.ApiURL +"/members/loginstatus/"+postdata,
                method: "GET"
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        updatepasswordfirstlogin: function(postdata, callback){
            $http({
                url: Config.ApiURL +"/members/updatepasswordfirstlogin",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(postdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        addsecurityquestion: function(postdata, callback){
            $http({
                url: Config.ApiURL +"/members/addsecurityquestion",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(postdata)
            }).success(function (data, status, headers, config) {
                callback(data);
            }).error(function (data, status, headers, config) {
                callback(data);
            });
        },

        setNav: function(){
            $http({
                url: "/v1/system/management/navigation",
                method: "get"
            }).success(function (data, status, headers, config) {
                store.set('mynav', data.records);
            }).error(function (data, status, headers, config) {
                callback(false,data);
            });
        },

        getNav: function(){
            return store.get('mynav');
        },

        getNav: function(){
            return store.get('mynav');
        }
    }
})
