'use strict';
/* Controllers */
app.controller('loginCtrl', function($scope, $window, Config, store, loginFctry, AuthSvc, errorService, timerService, $timeout) {

 

    $scope.error = null;
    $scope.timer = "";
    $scope.load = false;
    //Watching System Error Variables
    $scope.$watch(function() {
        return errorService.getError('system');
    }, function(newValue, oldValue) {
        $scope.error = newValue;
    }, true);
    //Watching System Error Variables
    $scope.$watch(function() {
        return timerService.timer;
    }, function(newValue, oldValue) {
        $scope.timer = newValue;
    }, true);
    $scope.close = function() {
        errorService.setError('system', null);
    }
    /*CHK IF LGIN USR*/
    if (AuthSvc.isTokenValid() == "expired") {
        return AuthSvc.requestRefreshToken().then(function successCallback(res) {
            $window.location.href = '/dashboard';
        }, function errorCallback(response) {
            AuthSvc.purgeTokens();
            AuthSvc.execExit("/admin");
        });
    } else if (AuthSvc.isTokenValid() == "valid") {
        $window.location.href = '/dashboard';
    }
    $scope.memberlogin = function(memberdata) {
        $scope.load = true;
        console.log("THIS SHOULD BE HITTED");
        AuthSvc.login(memberdata, true, function(status, res) {
            if (!status) {
                errorService.setError('system', res.records.userMessage);
                if (res.records.errorCode == 403) {
                    var t = new Date();
                    var tt = new Date(t.getTime() + (1000 * res.records.more.diff));
                    timerService.initializeClock(tt);
                }
            }
            $scope.load = false;
        })
    }
});